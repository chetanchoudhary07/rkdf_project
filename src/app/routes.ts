import { Routes } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { QpCouseCatComponent } from './qp-couse-cat/qp-couse-cat.component';
import { QpBESemComponent } from './qp-besem/qp-besem.component';



export const routes : Routes = [
    {path : 'home', component : HomeComponent},
    {path : 'about', component : AboutComponent},
    {path : 'contact', component : ContactComponent},
    {path : 'qpcoursecat', component : QpCouseCatComponent},
    {path : 'qpBESem', component : QpBESemComponent},
    {path : '', redirectTo : '/home', pathMatch : 'full'}    
];