import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QpCouseCatComponent } from './qp-couse-cat.component';

describe('QpCouseCatComponent', () => {
  let component: QpCouseCatComponent;
  let fixture: ComponentFixture<QpCouseCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QpCouseCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QpCouseCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
