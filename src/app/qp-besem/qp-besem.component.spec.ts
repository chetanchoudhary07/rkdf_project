import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QpBESemComponent } from './qp-besem.component';

describe('QpBESemComponent', () => {
  let component: QpBESemComponent;
  let fixture: ComponentFixture<QpBESemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QpBESemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QpBESemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
